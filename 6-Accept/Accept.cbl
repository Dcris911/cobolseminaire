      ******************************************************************
      * Author: David Gilbert
      * Date: 18 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
      *    Syntaxe : ACCEPT identifiant/variable FROM source
      *    Exemple:
      *        ACCEPT date-courrante FROM DATE
      *        ACCEPT temps-courrant FROM TIME
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. ACCEPT-PRACTICE.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
           01 NOM PIC A(10).

           01 JOUR-DE-SEMAINE PIC 9.

       PROCEDURE DIVISION.
       MAIN-PROCEDURE.
      *    Lire du clavier
           ACCEPT NOM.
           DISPLAY NOM

      *    Lire Fonction de COBOL
           ACCEPT JOUR-DE-SEMAINE FROM DAY-OF-WEEK.
           DISPLAY JOUR-DE-SEMAINE.

            STOP RUN.
       END PROGRAM ACCEPT-PRACTICE.
