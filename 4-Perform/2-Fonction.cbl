      ******************************************************************
      * Author: David Gilbert
      * Date: 18 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. FONCTION-PERFORM.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
           77 A PIC 9.
       PROCEDURE DIVISION.
           SET A TO 0.

       MEHTOD-MAUVAIS-ENDROIT.
           ADD 1 TO A.
      *    MEHTOD-MAUVAIS-ENDROIT FINI ICI

       MAIN-PROCEDURE.
           PERFORM MEHTOD-MAUVAIS-ENDROIT
           DISPLAY "A vaut 1 ou 2? A:" A.
           PERFORM FONCTION-1.
           PERFORM FONCTION-2 2 TIMES.
           DISPLAY "A:" A.
           STOP RUN.

       FONCTION-1.
           ADD 1 TO A.
           DISPLAY "Bonjour le monde".
      *    FONCTION-1 FINI ICI
       FONCTION-2.
           ADD 1 TO A.
           DISPLAY "FONCTION-2".
      *    FONCTION-2 FINI ICI
       END PROGRAM FONCTION-PERFORM.
