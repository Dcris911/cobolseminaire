      ******************************************************************
      * Author: David Gilbert
      * Date: 18 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
      *    Syntaxe :
      *    PERFORM PARA/SECTION [WITH TEST BEFORE/AFTER] UNTIL condition
      *        code
      *    END-PERFORM
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. BOUCLE-WHILE.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
           77 A PIC 9.
       PROCEDURE DIVISION.
           SET A TO 0.
       MAIN-PROCEDURE.
           PERFORM FONCTION-1 UNTIL A > 3.
           DISPLAY "Fini!"
           STOP RUN.

       FONCTION-1.
           ADD 1 TO A
           DISPLAY A.
       END PROGRAM BOUCLE-WHILE.
