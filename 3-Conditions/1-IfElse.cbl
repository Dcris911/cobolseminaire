      ******************************************************************
      * Author: David Gilbert
      * Date: 17 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. IFELSE.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
           77 X PIC 9.
       PROCEDURE DIVISION.
           SET X TO 0.
       MAIN-PROCEDURE.
           PERFORM 3 TIMES
               ADD 1 TO X GIVING X
               IF X = 3 THEN
                   DISPLAY "Hola!"
               ELSE
                   DISPLAY "X="X
               END-IF
           END-PERFORM
            STOP RUN.
       END PROGRAM IFELSE.
