      ******************************************************************
      * Author: David Gilbert
      * Date: 17 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. CONDITIONIF.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
           77 X PIC 9.
       PROCEDURE DIVISION.
           SET X TO 0.

       MAIN-PROCEDURE.
           PERFORM 3 TIMES
               ADD 1 TO X GIVING X
               DISPLAY "X="X
               IF X = 3 THEN
                   DISPLAY "Hola!"
               END-IF
           END-PERFORM
            STOP RUN.
       END PROGRAM CONDITIONIF.
