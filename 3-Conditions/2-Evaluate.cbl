      ******************************************************************
      * Author: David Gilbert
      * Date: 18 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
      *    Utile quand l'on a plusieurs conditions pour le meme if
      *        (clean code). Similaire � un Switch
      ******************************************************************
      *    Syntaxe:
      *        EVALUATE variable/etat
      *            WHEN cas1 [ALSO] cas2 ...
      *                code
      *            WHEN cas5 [ALSO] cas6
      *                code
      *            WHEN OTHER
      *                code
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. EVALUATE-SEMINAIRE.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
           77 A PIC 9.
           77 B PIC X.
       PROCEDURE DIVISION.
       MAIN-PROCEDURE.
      *    Exemple Basique
           ACCEPT A.
           ACCEPT B.
           EVALUATE TRUE
               WHEN A = 6
                   DISPLAY "6"
               WHEN A = 8
                   DISPLAY "8"
               WHEN OTHER
                   DISPLAY "OTHER"
           END-EVALUATE

           EVALUATE A
               WHEN 8
                   DISPLAY "8"
               WHEN 1 THRU 3
                   DISPLAY "THRU"
               WHEN 4
               WHEN 5
                   DISPLAY "4-5"
               WHEN OTHER
                   DISPLAY "OTHER"
           END-EVALUATE

      *    Multiples conditions
           EVALUATE A ALSO B
               WHEN 1 ALSO 'A'
                   DISPLAY "1-A"
               WHEN 2 ALSO 'B'
                   DISPLAY "2-B"
           END-EVALUATE

            STOP RUN.
       END PROGRAM EVALUATE-SEMINAIRE.
