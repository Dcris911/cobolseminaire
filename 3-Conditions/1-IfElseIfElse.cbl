      ******************************************************************
      * Author: David Gilbert
      * Date: 17 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. IFELSEIFELSE.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
           77 X PIC 9.
       PROCEDURE DIVISION.
           SET X TO 0.

       MAIN-PROCEDURE.
           PERFORM 5 TIMES
               ADD 1 TO X GIVING X
               IF X = 2
                       DISPLAY "LE X VAUT DEUX"
               ELSE
                   IF X = 4 THEN
                       DISPLAY "LE X VAUT QUATRE"
                   ELSE
                       DISPLAY "X"X
                   END-IF
               END-IF
           END-PERFORM
            STOP RUN.
       END PROGRAM IFELSEIFELSE.
