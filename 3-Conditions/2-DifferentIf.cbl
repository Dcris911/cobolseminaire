      ******************************************************************
      * Author: David Gilbert
      * Date: 18 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. DIFFERENT-CONDITION.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
           77 A PIC 9.
       PROCEDURE DIVISION.
           MOVE 9 TO A.
       MAIN-PROCEDURE.
           IF A = 9
               DISPLAY "9"
           END-IF
           IF A > 3
               DISPLAY "PLUS GRAND"
           END-IF
           IF A IS POSITIVE
               DISPLAY "POSITIVE"
           END-IF
           IF A IS NUMERIC
               DISPLAY "NUMERIC"
           END-IF
           IF A IS ALPHABETIC
               DISPLAY "LETTER"
           END-IF
            STOP RUN.
       END PROGRAM DIFFERENT-CONDITION.
