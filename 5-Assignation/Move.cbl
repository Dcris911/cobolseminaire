      ******************************************************************
      * Author:
      * Date:
      * Purpose:
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. EXEMPLEMOVE.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
           77 A PIC 999.
           77 X PIC X(20).

           77 B PIC 9999999.
       PROCEDURE DIVISION.
       MAIN-PARAGRAPH.
           MOVE 456 TO A
           MOVE "Je suis un etudiant" TO X
           DISPLAY A " | " X.

           *>(Position:NombreDeCharactere)
           *> Position commence a 1 et non pas 0
           MOVE A(1:2) TO B
           DISPLAY B " | " A.
           SET A TO 678
           DISPLAY B " | " A.
           MOVE A(1:2) TO B(2:2)
           DISPLAY B.
            STOP RUN.
       END PROGRAM EXEMPLEMOVE.
