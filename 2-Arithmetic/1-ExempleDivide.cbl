      ******************************************************************
      * Author: David Gilbert
      * Date: 16 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. EXEMPLE-DIVISION.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
           77 X PIC 9999.
           77 Y PIC 99.
           77 Z PIC 99.
           77 A PIC 99.
           77 B PIC 9.
           77 C PIC 9V99.
       PROCEDURE DIVISION.
           SET X TO 1316.
           SET Y TO 28.
           SET A TO 19.
           SET B TO 6.
       MAIN-PROCEDURE.
            DIVIDE X BY Y GIVING Z.
            DISPLAY X "/"Y "="Z.

            DIVIDE A BY B GIVING Z.
            DISPLAY A "/"B "="Z.
            DIVIDE A BY B GIVING C.
            DISPLAY A "/"B "="C.

            STOP RUN.
       END PROGRAM EXEMPLE-DIVISION.
