      ******************************************************************
      * Author: David Gilbert
      * Date: 18 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
      *    Syntaxe Basique:
      *        COMPUTE variable [ROUNDED] = valeur [+,-,*,/,**]
      *            [[NOT] ON SIZE ERROR
      *     ** -> Exponentiel
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. COMPUTE-SEMINAIRE.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
           77 A PIC 9V99.
           77 B PIC 9V99.
           77 C PIC 9.
       PROCEDURE DIVISION.
       MAIN-PROCEDURE.
           COMPUTE A = 15 - 10
           DISPLAY "A: " A.

           COMPUTE A = 5 / 3
           COMPUTE B ROUNDED = 5 / 3
           DISPLAY "A|B: " A " | " B.

           COMPUTE A = (5 * 15)/(20 - 10)
           DISPLAY "A: " A.

           COMPUTE C = (5 * 15)/(20 - 10)
               NOT ON SIZE ERROR DISPLAY "Probleme".
           DISPLAY "C: " C.
            STOP RUN.
       END PROGRAM COMPUTE-SEMINAIRE.
