      ******************************************************************
      * Author: David Gilbert
      * Date: 16 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. EXEMPLE-MULTIPLICATION.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
           77 X PIC 9.
           77 Y PIC 9.
           77 A PIC 9.
           77 B PIC 99.
       PROCEDURE DIVISION.
           SET X TO 9.
           SET Y TO 2.
       MAIN-PROCEDURE.
           MULTIPLY X BY Y GIVING A
           MULTIPLY X BY Y GIVING B
            DISPLAY "A="A" B="B
            STOP RUN.
       END PROGRAM EXEMPLE-MULTIPLICATION.
