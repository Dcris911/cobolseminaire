      ******************************************************************
      * Author: David Gilbert
      * Date: 16 mai 2018
      * Purpose: Un exemple d'addition
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. EXEMPLE-SOUSTRACTION.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
           77 X PIC 9.
           77 Y PIC 99.
           77 A PIC 99.
           77 B PIC 999.
           77 C PIC +99.
       PROCEDURE DIVISION.
           SET X TO 5.
           SET Y TO 25.
       MAIN-PROCEDURE.
           SUBTRACT X FROM Y GIVING A.
           SUBTRACT Y FROM X GIVING B.
           SUBTRACT Y FROM X GIVING C.
           DISPLAY Y "-" X "=" A.
           DISPLAY X "-" Y "=" B " (Supposer etre negatif)".
           DISPLAY X "-" Y "=" C.
           STOP RUN.
       END PROGRAM EXEMPLE-SOUSTRACTION.
