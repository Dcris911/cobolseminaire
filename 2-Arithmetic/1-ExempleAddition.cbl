      ******************************************************************
      * Author: David Gilbert
      * Date: 16 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. EXEMPLE-ADDITION.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
           77 X PIC 99.
           77 Y PIC 99.
           77 Z PIC 99.
       PROCEDURE DIVISION.
           SET X TO 5.
           SET Y TO 25.
       MAIN-PROCEDURE.
           ADD X TO Y GIVING Z.
           DISPLAY "X+Y=Z"
           DISPLAY X "+" Y "=" Z
           STOP RUN.
       END PROGRAM EXEMPLE-ADDITION.
