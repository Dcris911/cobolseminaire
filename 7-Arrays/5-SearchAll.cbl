      ******************************************************************
      * Author: David Gilbert
      * Date: 20 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. SEARCH-EXEMPLE.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
           01 TABLEAU.
               05 PRODUIT OCCURS 15 TIMES ASCENDING KEY IS
               VALEUR INDEXED BY INDX.
                   10 VALEUR PIC 99.
           77 K PIC 99.
       PROCEDURE DIVISION.
       MAIN-PROCEDURE.
            SET INDX TO 1.
            SET K TO 0.
            PERFORM BOUCLE-FONCTION UNTIL INDX > 15.
            PERFORM SEARCH-FONCTION.
            STOP RUN.
       BOUCLE-FONCTION.
           MOVE K TO VALEUR(INDX)
           ADD 1 TO K.
           SET INDX UP BY 1.
       SEARCH-FONCTION.
           SEARCH ALL PRODUIT
               AT END DISPLAY "VALEUR NON TROUVER"
               WHEN VALEUR(INDX) = 09
                   DISPLAY "Trouver valeur " VALEUR(INDX)
                   " a l'Indexe " INDX
           END-SEARCH.
       END PROGRAM SEARCH-EXEMPLE.
