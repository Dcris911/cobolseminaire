      ******************************************************************
      * Author: David Gilbert
      * Date: 17 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. VARIABLE-EXEMPLE.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.

           77 NOMBREVERSION2 PIC 9(3).
           77 NOMBREVIRGULE  PIC 9(3)v9(2).

           77 NOMBRE          PIC 999.
           77 CHENECARACTERE  PIC X(3).
           77 JUSTECHARACTERE PIC A(3).

       PROCEDURE DIVISION.
       MAIN-PROCEDURE.
           DISPLAY NOMBRE "|" NOMBREVERSION2 "|" NOMBREVIRGULE.
           DISPLAY CHENECARACTERE "|" JUSTECHARACTERE.

           MOVE  111  TO NOMBRE.
           MOVE 'ASD' TO CHENECARACTERE
           MOVE 'ASD' TO JUSTECHARACTERE
           DISPLAY NOMBRE "|" CHENECARACTERE "|" JUSTECHARACTERE.

      *     MOVE 'ZZZ' TO NOMBRE
      *     MOVE  111  TO CHENECARACTERE
      *     MOVE  111  TO JUSTECHARACTERE.

           DISPLAY NOMBRE "|" CHENECARACTERE
            STOP RUN.
       END PROGRAM VARIABLE-EXEMPLE.
