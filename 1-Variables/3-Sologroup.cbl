      ******************************************************************
      * Author: David Gilbert
      * Date: 18 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. VARIABLE-INDIVIDUEL-ET-GROUPER.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
      *    Item Elementaire
           01 NOMBRE PIC 9(2).

      *    Item grouper
           01 EMPLOYEE.
               05 EMPLOYEE-ID PIC 9(5).
               05 EMPLOYEE-NAME.
                   10 FIRST-NAME PIC A(5).
                   10 LAST-NAME  PIC A(7).
               05 TEST-DISPLAY   PIC A(4).
               05 SECOND-TEST    PIC A(2).
      *    Renommer des champs
           66 EMPLOYEE-STATUS RENAMES EMPLOYEE-ID THRU EMPLOYEE-NAME.
           66 TEST-STATUS RENAMES EMPLOYEE-ID THRU TEST-DISPLAY.


       PROCEDURE DIVISION.
       MAIN-PROCEDURE.
           MOVE 1 TO EMPLOYEE-ID
           MOVE 'DAVID' TO FIRST-NAME
           MOVE 'GILBERT' TO LAST-NAME
           MOVE 'TEST' TO TEST-DISPLAY
           MOVE 'UU' TO SECOND-TEST.
           DISPLAY "ID-FIRSTNAME-LASTNAME-TEST-TEST2: "
               EMPLOYEE-ID "-" FIRST-NAME "-"
               LAST-NAME "-" TEST-DISPLAY "-" SECOND-TEST.
           DISPLAY "EMPLOYEE NAME  : " EMPLOYEE-NAME
           DISPLAY "EMPLOYEE       : " EMPLOYEE
           DISPLAY "EMPLOYEE-DETAIL: " EMPLOYEE-STATUS
           DISPLAY "TEST-STATUS    : " TEST-STATUS
            STOP RUN.
       END PROGRAM VARIABLE-INDIVIDUEL-ET-GROUPER.
