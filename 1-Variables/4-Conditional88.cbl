      ******************************************************************
      * Author: David Gilbert
      * Date: 18 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. CONDITIONAL.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
           01 JOUR-DE-LA-SEMAINE-COURANTE PIC 9.
      *    QUAND LA VALEUR JOUR-DE-SEMAINE EST SETTER A
      *     UN DES JOUR DE LA SEMAINE
      *    LE JOUR CHOISI EST A TRUE ET LES AUTRES SONT A FALSE
           01 VERIFICATION-JOUR.
               05 JOUR-DE-SEMAINE PIC 9.
                   88 LUNDI    VALUE 1.
                   88 MARDI    VALUE 2.
                   88 MERCREDI VALUE 3.
                   88 JEUDI    VALUE 4.
                   88 VENDREDI VALUE 5.
                   88 SAMEDI   VALUE 6.
                   88 DIMANCHE VALUE 7.
       PROCEDURE DIVISION.
       MAIN-PROCEDURE.
           ACCEPT JOUR-DE-LA-SEMAINE-COURANTE FROM DAY-OF-WEEK.
           MOVE JOUR-DE-LA-SEMAINE-COURANTE TO JOUR-DE-SEMAINE
      *    Verifie quel jour on est
           IF LUNDI
               DISPLAY "LUNDI"
           END-IF
           IF MARDI
               DISPLAY "MARDI"
           END-IF
           IF MERCREDI
               DISPLAY "MERCREDI"
           END-IF
           IF JEUDI
               DISPLAY "JEUDI"
           END-IF
           IF VENDREDI
               DISPLAY "VENDREDI"
           END-IF

            STOP RUN.
       END PROGRAM CONDITIONAL.
